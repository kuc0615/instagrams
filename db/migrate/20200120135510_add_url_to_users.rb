class AddUrlToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :url, :text
  end
end
