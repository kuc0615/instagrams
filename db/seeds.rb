
User.create!(name:  "Ruby",
             username: "Rails",
             email: "ruby@rails.com",
             password:              "example",
             password_confirmation: "example",
             activated: true,
             activated_at: Time.zone.now)

49.times do |n|
  name  = Faker::Name.name
  username = Faker::Name.name
  email = "example-#{n+1}@rails.com"
  password = "password"
  User.create!(name:  name,
               username: username,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
40.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content, picture: open("#{Rails.root}/db/fixtures/images/image.png"))}
end

users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }