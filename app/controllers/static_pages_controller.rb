class StaticPagesController < ApplicationController
  
  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end
  
  def search
  end
  
  def searching
    if params[:search][:content] == ''
      render 'search'
    else
      @microposts = Micropost.where("content LIKE ?","%#{params[:search][:content]}%")
      render 'search'
    end
  end
  
  def terms
  end
  
end
