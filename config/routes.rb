
Rails.application.routes.draw do

  get '/search',            to:'static_pages#search'
  post '/search',             to:'static_pages#searching'
  get 'password_resets/new'
  get 'password_resets/edit'
  get 'sessions/new'
  root 'static_pages#home'
  get  '/terms', to: 'static_pages#terms'
  get '/signup', to:'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets, only: [:new, :create, :edit, :update]
  resources :microposts, only: [:show, :create, :destroy]
  resources :comments,            only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :likes, only: [:create, :destroy]
end
